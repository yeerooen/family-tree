const mongoose = require("mongoose");
const { Schema } = mongoose;

const deathEventSchema = new Schema({
    profile: String,
    date: Number,
    location: String,
    time: String
});

mongoose.model("deathEvent", deathEventSchema);
