const mongoose = require("mongoose");
const { Schema } = mongoose;

const birthEventSchema = new Schema({
    profile: String,
    date: Number,
    location: String,
    time: String
});

mongoose.model("birthEvent", birthEventSchema);
