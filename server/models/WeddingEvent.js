const mongoose = require("mongoose");
const { Schema } = mongoose;

const weddingEventSchema = new Schema({
    profile: String,
    date: Number,
    location: String,
    time: String,
    bride: String
});

mongoose.model("weddingEvent", weddingEventSchema);
