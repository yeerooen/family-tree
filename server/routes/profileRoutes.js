const mongoose = require("mongoose");

const Profile = mongoose.model("profiles");
const DeathEvent = mongoose.model("deathEvent");
const BirthEvent = mongoose.model("birthEvent");
const WeddingEvent = mongoose.model("weddingEvent");

module.exports = app => {
    app.post("/api/profile/create", async (req, res) => {
        const { name, birth, death } = req.body;
        await new Profile({ name, birth, death }).save();
        res.send(req.user);
    });

    app.post("/api/profile/search", async (req, res) => {
        let profile;
        if (req.body.search) {
            profile = await Profile.find({
                name: { $regex: req.body.search, $options: "i" }
            }).exec();
        } else {
            profile = await Profile.find().exec();
        }
        res.send(profile);
    });

    app.post("/api/profile/load", async (req, res) => {
        const { userId } = req.body;
        const user = await Profile.findById(userId);
        const profile = {
            name: user.name,
            birth: user.birth,
            death: user.death,
            id: userId
        };
        res.send(profile);
    });

    app.post('/api/events/birth/create', async (req, res) => {
        console.log(req.body);
        await new BirthEvent(req.body).save();
        res.send(req.user);
    });

    app.post('/api/events/birth/load', async (req, res) => {
        const { profile } = req.body;
        const birthEvents = await BirthEvent.find({ profile });
        res.send(birthEvents);
    });

    app.post('/api/events/death/create', async (req, res) => {
        await new DeathEvent(req.body).save();
        res.send(req.user);
    });

    app.post('/api/events/death/load', async (req, res) => {
        const { profile } = req.body;
        const deathEvents = await DeathEvent.find({ profile });
        res.send(deathEvents);
    });

    app.post('/api/events/wedding/create', async (req, res) => {
        console.log(req.body);
        await new WeddingEvent(req.body).save();
        res.send(req.user);
    });

    app.post('/api/events/wedding/load', async (req, res) => {
        const { profile } = req.body;
        const weddingEvents = await WeddingEvent.find({ profile });
        res.send(weddingEvents);
    });

};
