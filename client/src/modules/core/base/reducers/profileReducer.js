import {
    SET_PROFILE_POPUP,
    SET_PROFILE_SEARCH_RESULT,
    SET_CURRENT_PROFILE,
    ADD_EVENTS,
    CLEAR_EVENTS
} from '../actions/types'

const initialState = {
    profilePopup: "test",
    profileSearchResults: [],
    currentProfile: '',
    events: []
};

export default (state = initialState, action) => {

    switch (action.type) {
        case CLEAR_EVENTS:
            return { ...state, events: [] };
        case ADD_EVENTS:
            let events = [...state.events, ...action.state];
            events = events.sort((a, b) => a.date - b.date);
            return { ...state, events: events };
        case SET_CURRENT_PROFILE:
            return { ...state, currentProfile: action.state };
        case SET_PROFILE_SEARCH_RESULT:
            return { ...state, profileSearchResults: action.results };
        case SET_PROFILE_POPUP:
            return { ...state, profilePopup: action.state };
        default:
            return state;
    }

};