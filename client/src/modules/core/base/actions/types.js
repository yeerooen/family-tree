export const SET_PROFILE_POPUP = 'set_profile_popup';
export const FETCH_USER = 'fetch_user';
export const SET_PROFILE_SEARCH_RESULT = 'set_profile_search_result';
export const SET_CURRENT_PROFILE = 'set_current_profile';
export const ADD_EVENTS = 'add_events';
export const CLEAR_EVENTS = 'clear_events';