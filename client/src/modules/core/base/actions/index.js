import axios from "axios";
import {
    SET_PROFILE_POPUP,
    SET_PROFILE_SEARCH_RESULT,
    SET_CURRENT_PROFILE,
    ADD_EVENTS, CLEAR_EVENTS
} from "./types";

export const setPopup = state => dispatch => {
    dispatch({ type: SET_PROFILE_POPUP, state });
};

export const setCurrentProfile = state => dispatch => {
    dispatch({ type: SET_CURRENT_PROFILE, state });
}

export const handleProfileCreation = data => async dispatch => {
    const res = await axios.post("/api/profile/create", data);
    dispatch({ type: SET_PROFILE_POPUP, state: "" });
};

export const loadProfiles = data => async dispatch => {
    const res = await axios.post("/api/profile/search", data);
    dispatch({ type: SET_PROFILE_SEARCH_RESULT, results: res.data });
};

export const getUserData = userId => async dispatch => {
    const res = await axios.post("/api/profile/load", { userId });
    const user = {
        user: {
            name: res.data.name,
            id: userId
        }
    };
    console.log(user);
    return user;
};

export const addBirth = formData => async dispatch => {
    await axios.post('/api/events/birth/create', formData);
    dispatch({ type: SET_PROFILE_POPUP, state: "" });
};

export const loadBirth = profile => async dispatch => {
    const birthEvents = await axios.post('/api/events/birth/load', profile);
    const newValue = birthEvents.data.map(event => ({
        date: event.date,
        color: '#c4eca4',
        _: [
            {
                title: 'Gebeurtenis',
                value: 'Geboorte'
            }, {
                title: 'Locatie',
                value: event.location
            }, {
                title: 'Tijd',
                value: event.time
            },
        ]
    }));
    dispatch({ type: ADD_EVENTS, state: newValue });
}

export const addDeath = formData => async dispatch => {
    await axios.post('/api/events/death/create', { ...formData, color: '#ff00000', type: 'Overlijden' });
    dispatch({ type: SET_PROFILE_POPUP, state: "" });
};

export const loadDeath = profile => async dispatch => {
    const deathEvents = await axios.post('/api/events/death/load', profile);
    const newValue = deathEvents.data.map(event => ({
        date: event.date,
        color: '#ff0000',
        _: [
            {
                title: 'Gebeurtenis',
                value: 'Overlijden'
            }, {
                title: 'Locatie',
                value: event.location
            }, {
                title: 'Tijd',
                value: event.time
            },
        ]
    }));
    dispatch({ type: ADD_EVENTS, state: newValue });
}

export const addWedding = formData => async dispatch => {
    await axios.post('/api/events/wedding/create', { ...formData });
    loadWedding({ profile: formData.profile })
    dispatch({ type: SET_PROFILE_POPUP, state: "" });
};

export const loadWedding = profile => async dispatch => {
    const weddingEvents = await axios.post('/api/events/wedding/load', profile);
    weddingEvents.data.map(async event => {
        const bride = await axios.post('/api/profile/load', { userId: event.bride });
        dispatch({
            type: ADD_EVENTS, state:
                [{
                    date: event.date,
                    color: '#ff8888',
                    _: [
                        {
                            title: 'Gebeurtenis',
                            value: 'Bruiloft'
                        }, {
                            title: 'Locatie',
                            value: event.location
                        }, {
                            title: 'Tijd',
                            value: event.time
                        }, {
                            title: 'Bruid',
                            value: bride.data.name,
                            link: '/profile/' + bride.data.id
                        }
                    ]
                }]
        });
    });
}

export const clearEvents = () => dispatch => {
    dispatch({ type: CLEAR_EVENTS });
}