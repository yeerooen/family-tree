import React from "react";
import { Link } from 'react-router-dom'
import { withRouter } from 'react-router-dom';

const ProfileEvent = props => {

    return (
        <div className="event-item">
            <div className="event-item__circle" style={{ backgroundColor: props.circleColor }}></div>
            <div className="event-item__date">{props.date}</div>
            <table>
                <tbody>
                    {props.content.map((content, index) => {

                        return (
                            <tr key={index}>
                                <td className="event-item__title">{content.title}</td>
                                <td className="event-item__value">
                                    {content.link ?
                                        <a href={content.link}> {content.value}</a>
                                        :
                                        content.value
                                    }
                                </td>

                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </div>
    );
}

export default withRouter(ProfileEvent);